--[[------------
  Q U A K E II
Heads Up Display
Main HUD Elements
]]--------------

-- Includes
include("damage.lua");
include("ammo.lua");
include("clip.lua");
include("health.lua");
include("armor.lua");
include("frags.lua");
