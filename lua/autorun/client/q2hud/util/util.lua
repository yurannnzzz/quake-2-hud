--[[------------
  Q U A K E II
Heads Up Display
   Utilities
]]--------------

-- Includes
include("sprites.lua");
include("numbers.lua");
include("weapons.lua");
include("ammo.lua");
include("chars.lua");
